import aguiaj.iscte.ColorImage;

public class OrganizadorFotografias {
	public static final int MAX_NUM_FOTOS = 10;

	private Fotografia fotografiaEmExibicao;
	private Fotografia[] fotografias;

	private int indiceFotografiaEmExibicao;
	private int indiceUltimaFotografia;


	public OrganizadorFotografias(Fotografia fotografia) {
		this.fotografiaEmExibicao = fotografia;
		this.fotografias = new Fotografia [MAX_NUM_FOTOS];
		this.indiceUltimaFotografia = -1;

		adicionarFotografia(fotografia);
		this.indiceFotografiaEmExibicao = 0;
	}

	public boolean adicionarFotografia(Fotografia fotografia) {
		if(this.indiceUltimaFotografia < (MAX_NUM_FOTOS-1)) {
			this.fotografias[this.indiceUltimaFotografia++] = fotografia;
			return true;
		}
		return false;
	}

	public boolean inserirFotografia(Fotografia fotografia) {
		if(this.indiceUltimaFotografia < (MAX_NUM_FOTOS-1)) {
			for(int i = this.indiceUltimaFotografia; i <= this.indiceFotografiaEmExibicao; i++) {
				this.fotografias[i+1] = this.fotografias[i];
			}
			this.fotografias[indiceFotografiaEmExibicao] = fotografia;
			this.indiceUltimaFotografia++;
			this.fotografiaEmExibicao = fotografia;
		}
		return false;
	}

	public void removerFotografiaEmExibicao() {
		if(this.indiceUltimaFotografia > 0) {
			if(this.indiceFotografiaEmExibicao == this.indiceUltimaFotografia) {
				this.fotografiaEmExibicao = this.fotografias[this.indiceFotografiaEmExibicao--];
			}
			else {
				this.fotografiaEmExibicao = this.fotografias[this.indiceFotografiaEmExibicao++];
			}
		}
	}

	public ColorImage obterImagemFotografiaEmExibicao() {
		return this.fotografiaEmExibicao.ObterImagem();
	}

	public ColorImage obterFotografiaAnteriorReduzida() {
		if(this.indiceUltimaFotografia > 0) {
			int indiceAnterior = (this.indiceFotografiaEmExibicao - 1) % MAX_NUM_FOTOS;
			ColorImage imagem = this.fotografias[indiceAnterior].ObterImagem();
			return ManipulaImagem.reduzirTamanho(imagem, 2);
		}
		ColorImage imagem = this.fotografias[0].ObterImagem();
		return ManipulaImagem.reduzirTamanho(imagem, 2);
	}

	public ColorImage obterFotografiaSeguinteReduzida() {
		if(this.indiceUltimaFotografia > 0) {
			int indiceSeguinte = (this.indiceFotografiaEmExibicao + 1) % MAX_NUM_FOTOS;
			ColorImage imagem = this.fotografias[indiceSeguinte].ObterImagem();
			return ManipulaImagem.reduzirTamanho(imagem, 2);
		}
		ColorImage imagem = this.fotografias[0].ObterImagem();
		return ManipulaImagem.reduzirTamanho(imagem, 2);
	}

	public void mudarFotografiaEmExibicaoParaSeguinte() {
		if(this.indiceUltimaFotografia > 0) {
			int indiceSeguinte = (this.indiceFotografiaEmExibicao + 1) % MAX_NUM_FOTOS;
			this.fotografiaEmExibicao = this.fotografias[indiceSeguinte];
			this.indiceFotografiaEmExibicao = indiceSeguinte;
		}
	}

	public void mudarFotografiaEmExibicaoParaAnterior() {
		if(this.indiceUltimaFotografia > 0) {
			int indiceAnterior = (this.indiceFotografiaEmExibicao - 1) % MAX_NUM_FOTOS;
			this.fotografiaEmExibicao = this.fotografias[indiceAnterior];
			this.indiceFotografiaEmExibicao = indiceAnterior;
		}
	}
	
	public Fotografia[] obterFotografiasComEtiqueta(String etiqueta) {
		Fotografia[] vFotografias = new Fotografia [this.indiceUltimaFotografia+1];
		for(int i = 0; i <= this.indiceFotografiaEmExibicao; i++) {
			ColorImage imagem = this.fotografias[i].ObterImagem();
			String[] etiquetas = {etiqueta};
			vFotografias[i] = new Fotografia(imagem, etiquetas);
		}
		return vFotografias;
	}
	
	public ColorImage ObterImagemEmExibicaoAmpliada() {
		ColorImage imagem = this.fotografiaEmExibicao.ObterImagem();
		return ManipulaImagem.aumentarTamanho(imagem);
	}
	
	public void PixelizarImagemEmExibicao() {
		ColorImage imagem = this.fotografiaEmExibicao.ObterImagem();
		String[] etiquetas = this.fotografiaEmExibicao.obterEtiquetas();
		ColorImage imagemPixelizada = ManipulaImagem.pixelizar(imagem, 3);
		this.fotografiaEmExibicao = new Fotografia(imagemPixelizada,etiquetas);
		
	}

	public OrganizadorFotografias(ColorImage[] imagens, String etiqueta) {
		// TODO: Criar vector de fotografias, todas com etiqueta fornecida
		//       Fotografia em exibicao e a 1a imagem vector
		//       Verificar limites (MAX_NUM_FOTOS)
		this.fotografias = new Fotografia [MAX_NUM_FOTOS];
		this.indiceUltimaFotografia = -1;
		
		for(int i = 0; i < MAX_NUM_FOTOS && i < imagens.length; i++) {
			String[] etiquetas = {etiqueta};
			Fotografia fotografia = new Fotografia(imagens[i],etiquetas);
			adicionarFotografia(fotografia);
		}
		this.fotografiaEmExibicao = this.fotografias[0];
		this.indiceFotografiaEmExibicao = 0;
	}
}
