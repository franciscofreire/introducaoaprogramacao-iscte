import aguiaj.iscte.ColorImage;

public class Fotografia {
	ColorImage imagem;
	String[] etiquetas;
	private int ultimoIndice;
	public final int MAXIMO = 4;

	public Fotografia(ColorImage image) {
		this.imagem = image;
		this.etiquetas = new String[MAXIMO];
		ultimoIndice = -1;
	}
	
	public Fotografia(ColorImage image, String[] etiquetas) {
		this(image); // uso do construtor que ja temos
		for(int i = 0; i < etiquetas.length && i < MAXIMO; i++) {
			this.etiquetas[i] = etiquetas[i];
			ultimoIndice++;
		}
	}

	public boolean NovaEtiqueta(String etiqueta) {
		if (ultimoIndice < MAXIMO-1) {
			this.etiquetas[ultimoIndice] = etiqueta;
			ultimoIndice++;
			return true;
		} else {
			return false;
		}
	}

	public boolean TemEtiqueta(String etiqueta) {
		for (int i = 0; i <= ultimoIndice; i++) {
			if (etiqueta.equals(etiquetas[i])) {
				return true;
			}
		}
		return false;
	}

	public ColorImage ObterImagem() {
		return this.imagem;
	}

	public String[] obterEtiquetas() {
		return this.etiquetas;
	}

	public String TodasAsEtiquetas() {
		String Resultado = new String();
		for (int i = 0; i <= ultimoIndice; i++) {
			Resultado = Resultado + this.etiquetas[i];
		}
		return Resultado;
	}
}