import aguiaj.iscte.Color;
import aguiaj.iscte.ColorImage;

public final class ManipulaImagem {
	
	// Para nao ser possivel criar instancias
	private ManipulaImagem() {
	
	}
	// constante a ser usada para aumentar 
	private static final int FACTOR_AUMENTO = 3;
	
	private static Color mediaPixeis(Color[] pixeis) {
		int somaR = 0;
		int somaG = 0;
		int somaB = 0;
		int numPixeis = pixeis.length; 
		
		for(int i=0; i < numPixeis; i++) {
			somaR += pixeis[i].getR();
			somaG += pixeis[i].getG();
			somaB += pixeis[i].getB();
		}
		int mediaR = somaR / numPixeis;
		int mediaG = somaG / numPixeis;
		int mediaB = somaB / numPixeis;
		
		return new Color(mediaR, mediaG, mediaB);
	}
	
	// notas: (y,x) sao coordenadas do ponto superior esquerdo
	private static Color[] obterDivisao(ColorImage imagem, int y, int x, int factor) {
		Color[] divisao = new Color [factor*factor];
		int i = 0;
		for(int j = y; j < y + factor; j++) {
			for(int k = x; k < x + factor; k++) {
				divisao[i++] = imagem.getColor(k, j);
			}
		}
		return divisao;
	}
	
	private static boolean posicaoValida(ColorImage colorImage, int y, int x) {
		return y >= 0 && y < colorImage.getHeight() && 
				x >= 0 && x < colorImage.getWidth();
	}
	
	// pixeis adjacentes + proprio
	private static Color[] obterPixeisAdjacentes(ColorImage colorImage, int y, int x) {
		Color[] auxiliar = new Color [FACTOR_AUMENTO*FACTOR_AUMENTO];
		int i = 0;
		
		auxiliar[i++] = colorImage.getColor(x, y);
		
		if(posicaoValida(colorImage, y-1, x-1)) {
			auxiliar[i++] = colorImage.getColor(x-1, y-1);
		}
		
		if(posicaoValida(colorImage, y-1, x)) {
			auxiliar[i++] = colorImage.getColor(x, y-1);		
		}
		
		if(posicaoValida(colorImage, y-1, x+1)) {
			auxiliar[i++] = colorImage.getColor(x+1, y-1);
		}

		if(posicaoValida(colorImage, y, x-1)) {
			auxiliar[i++] = colorImage.getColor(x-1, y);
		}

		if(posicaoValida(colorImage, y, x+1)) {
			auxiliar[i++] = colorImage.getColor(x+1, y);
		}

		if(posicaoValida(colorImage, y+1, x-1)) {
			auxiliar[i++] = colorImage.getColor(x-1, y+1);
		}

		if(posicaoValida(colorImage, y+1, x)) {
			auxiliar[i++] = colorImage.getColor(x, y+1);
		}

		if(posicaoValida(colorImage, y+1, x+1)) {
			auxiliar[i++] = colorImage.getColor(x+1, y+1);
		}
		
		Color[] adjacentes = new Color [i];
		for(int j = 0; j < i; j++) {
			adjacentes[j] = auxiliar[i];
		}
		return adjacentes;
	}
	// notas: (y,x) sao coordenadas do ponto superior esquerdo
	private static void preencherDivisaoComCor(int[][][] matriz, int y, int x, int factor, Color color) {
		for(int i = y; i < y + factor; i++) {
			for(int j = x; j < x + factor; j++) {
				matriz[i][j][0] = color.getR();
				matriz[i][j][1] = color.getG();
				matriz[i][j][2] = color.getB();
			}
		}
	}
	
	public static ColorImage reduzirTamanho(ColorImage colorImage, int factor) {
		int height = colorImage.getHeight();
		int width = colorImage.getWidth();
		int[][][] newImageMatrix = null;
		if(height > factor && width > factor) {
			// caso em que e possivel dividir a imagem em quadrados factor X factor
			if(height % factor == 0 && width % factor == 0) {
				int newHeight = height / factor;
				int newWidth = width / factor;
				newImageMatrix = new int [newHeight][newWidth][3];
				
				for(int i = 0; i <= height-factor; i += factor) {
					for(int j = 0; j <= width-factor; j += factor) {
						Color[] divisao = obterDivisao(colorImage, i, j, factor);
						Color newColor = mediaPixeis(divisao);
						newImageMatrix[i/factor][j/factor][0] = newColor.getR(); 
						newImageMatrix[i/factor][j/factor][1] = newColor.getG();
						newImageMatrix[i/factor][j/factor][2] = newColor.getB();
					}
				}
			}
		}
		return ColorImage.fromMatrix(newImageMatrix);
	}
	
	public static ColorImage aumentarTamanho(ColorImage colorImage) {
		int height = colorImage.getHeight();
		int width = colorImage.getWidth();
		
		
		int[][][] newImageMatrix = new int [height*FACTOR_AUMENTO][width*FACTOR_AUMENTO][3];
		
		for(int i = 0; i < height; i++) {
			for(int j = 0; j <= width; j++) {
				Color[] adjacentes = obterPixeisAdjacentes(colorImage, i, j);
				Color mediaAdjacentes = mediaPixeis(adjacentes);
				preencherDivisaoComCor(newImageMatrix, i*FACTOR_AUMENTO, j*FACTOR_AUMENTO, 
						FACTOR_AUMENTO, mediaAdjacentes);
			}
		}
		
		return ColorImage.fromMatrix(newImageMatrix);
	}
	
	public static ColorImage pixelizar(ColorImage colorImage, int factor) {
		int height = colorImage.getHeight();
		int width = colorImage.getWidth();
		int[][][] newImageMatrix = new int [height][width][3];
		
		for(int i = 0; i < height; i++) {
			for(int j = 0; j <= width; j++) {
				newImageMatrix[i][j][0] = colorImage.getColor(j, i).getR();
				newImageMatrix[i][j][1] = colorImage.getColor(j, i).getG();
				newImageMatrix[i][j][2] = colorImage.getColor(j, i).getB();
			}
		}
		if(height >= factor && width >= factor) {
			// FIXME: Incluir este comportamento no ciclo anterior
			for(int i = 0; i <= height-factor; i += factor) {
				for(int j = 0; j <= width-factor; j += factor) {
					Color[] divisao = obterDivisao(colorImage, i, j, factor);
					Color newColor = mediaPixeis(divisao);
					preencherDivisaoComCor(newImageMatrix,i,j,factor,newColor);
				}
			}
		}
		return ColorImage.fromMatrix(newImageMatrix);
	}
}
